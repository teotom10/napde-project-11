%% Definition of the Problem
clear
problem = problem_get('heat_equation','heat.ini');
HFmod = problem.get_model(problem);

clear optGen_base
optGen_base.do_plot = 1;
optGen_base.do_save = 1;
optGen = optGen_base;

%% Model learning
dir = model_learn('opt_heat.ini')  

%% ANN model loading
ANNmod = read_model_fromfile(problem, dir);  
ANNmod.visualize();

%% ANN model test
test_solve.tt = [0 10]; %set time interval
test_solve.uu = @(t) [30+sin(t);15+0.*t;45+0.*t;...
                      95+0.*t;55+0.*t;19+0.*t;...
                      13+0.*t;48+0.*t;66+0.*t]; %set nU time-dependent input

figure();
output_HF = model_solve(test_solve,HFmod,struct('do_plot',1));
figure();
output_ANN = model_solve(test_solve,ANNmod,struct('do_plot',1));


%% Error evaluation
dataset_def.problem = problem;
dataset_def.type = 'file';
dataset_def.source = 'test.mat';
test_dataset = dataset_get(dataset_def);
model_compute_error(HFmod, test_dataset);
model_compute_error(ANNmod, test_dataset);
model_compute_error(ANNmod, test_dataset, optGen);
